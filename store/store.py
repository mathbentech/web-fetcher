#!/bin/usr/env python3
# -*- coding: utf-8 -*-

from urllib import request
import os
import shutil
from bs4 import BeautifulSoup


class Store:
    def __init__(self, url_data, name="Module Python Store", logger=None):
        self._url_data = url_data
        self.name = name
        # Use the created name
        self._tmp_dir_path = os.path.join(".", "result", name)
        self._tmp_index_file_path = os.path.join(self._tmp_dir_path, "index.html")
        self._txt = ""
        self._lst_product = []
        self._logger = logger

    def fetch_and_parse_data(self, clear_cache=False):
        self.fetch_data(clear_cache=clear_cache)
        self.parse_data()

    def parse_data(self):
        self._lst_product = []

        # Validation
        if not self._txt:
            if self._logger:
                self._logger.error("Cannot parse empty store data.")
            raise ValueError("")

        # Parse with BeautifulSoup
        html = BeautifulSoup(self._txt, "html.parser")

        # Use specific class
        # TODO why?
        lst_container = html.find_all("div", {"class": "jet-listing-grid__item"})
        for container in lst_container:
            # TODO need protocol documentation
            lst_info = container.text.strip().split('\n')
            name = lst_info[0]
            price = ""
            for info in lst_info:
                if "$" in info:
                    price = float(info[2:info.find(" ", 2)])

            lst_a = [p.get("href") for p in container.find_all("a") if "product/" in p.get("href")]
            url = lst_a[0]

            # Add new product
            self._lst_product.append((name, price, url))

        self._lst_product.sort(key=lambda x: x[1])

    def print_all(self):
        for name, price, url in self._lst_product:
            if self._logger:
                self._logger.info("%s - %s - %s" % (name, price, url))

    def fetch_data(self, clear_cache=False):
        if clear_cache:
            # Remove directory of caching files
            shutil.rmtree(self._tmp_dir_path, ignore_errors=True)

        if os.path.exists(self._tmp_index_file_path):
            with open(self._tmp_index_file_path) as data_file:
                txt = data_file.read()
                if self._logger:
                    self._logger.debug("Read html from cache %s" % self._tmp_index_file_path)
        else:
            txt = self._generate_tmp_file()
            os.makedirs(self._tmp_dir_path, exist_ok=True)

            with open(self._tmp_index_file_path, "w") as data_file:
                data_file.write(txt)
                if self._logger:
                    self._logger.debug("Create cache html %s" % self._tmp_index_file_path)

        self._txt = txt

    def _generate_tmp_file(self):
        if self._logger:
            self._logger.debug("Fetch website init.")
        try:
            f = request.urlopen(self._url_data)
        except Exception as e:
            if self._logger:
                self._logger.error(e)
            raise e
        txt = f.read().decode()
        if self._logger:
            self._logger.debug("Fetch website done.")
        return txt
