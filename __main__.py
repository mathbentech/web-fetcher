#!/bin/usr/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
from store.store import Store

MODULE_NAME = "HerboMobile"

logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s')

logger = logging.getLogger(MODULE_NAME)


def main():
    args = parse_args()
    if args.debug:
        logger.info("Arguments:%s" % args)

    store = Store(args.url, name=args.store_name, logger=logger)
    store.fetch_and_parse_data(args.clear_cache)
    store.print_all()


def parse_args():
    parser = argparse.ArgumentParser(description="""Herbo Mobil module, extract information from Websites.""")

    group = parser.add_argument_group("Debug")
    group.add_argument('-d', '--debug', default=False, action='store_true',
                       help='Enable debug.')

    group = parser.add_argument_group("Target")
    group.add_argument('--store_name', default=MODULE_NAME, help='Your store name.')
    group.add_argument('--url', default="", help='The URL of the website.')

    group = parser.add_argument_group("Fetching data")
    group.add_argument('--clear_cache', default=False, action='store_true',
                       help='Remove cache before fetching data. This will write a new cache.')

    _parser = parser.parse_args()

    return _parser


if __name__ == '__main__':
    main()
