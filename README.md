Web Fetcher
===========
Script to fetch data from your favorite website!

Supported store
===============
This information is not divulged to protect different stores.

DEPENDENCY
==========
Python 3.7

pip3
----
[See requirements.txt](requirements.txt)

With virtual environment
========================
Ubuntu INSTALL venv
-------------------
> sudo apt install python3-venv

Create virtual environment
> python3 -m venv venv
>
> venv/bin/pip3 install -r requirements.txt

Run
> venv/bin/python3 __main__.py

INSTALL
=======
Ubuntu
------
> sudo apt install python3
>
> pip3 install -r requirements.txt

RUN
===
> python3 __main__.py

License
=======
AGPLv3